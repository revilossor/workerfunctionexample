self.onmessage = function(e) {
	console.log('message received from main script');
}
function onTick() {
	console.log('worker tick!');
	self.postMessage('workerTick');
}


(function() {
	function init() {
		setInterval(onTick, 1000);
	}
	init();
})();