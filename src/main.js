(function() {
	var worker;
	
	function init() {
		if(window.Worker) {
			var functionBody = getFunctionBody(workerFunction);
			console.log('functionBody is ' + functionBody);
			var functionBlob = getJavascriptBlob(functionBody);
			console.log('functionBlob is ' + functionBlob);
			var functionBlobUri = URL.createObjectURL(functionBlob);
			console.log('functionBlobUri is ' + functionBlobUri);
			
			//worker = new Worker('./worker.js');		// usually done with a script file....
			worker = new Worker(functionBlobUri);
			worker.addEventListener('message', onMessage);
			
		} else {
			console.log('no web worker support!');
		}
	}
	
	function onMessage(e) {
		console.log('message from worker!');
		worker.postMessage('some data');
	}
	
	init();
	
})();

function getFunctionBody(func) {
	var stringifiedFunction = '.' + func;
	return stringifiedFunction.substring(stringifiedFunction.indexOf('{') + 1, stringifiedFunction.lastIndexOf('}'));
}
function getJavascriptBlob(of) {
	return new Blob([of], {type: 'application/javascript'});
}

function workerFunction() {	
	self.onmessage = function(e) {
		console.log('message received from main script');
	}
	function onTick() {
		console.log('worker tick!');
		self.postMessage('workerTick');
	}
	(function() {
		function init() {
			setInterval(onTick, 1000);
		}
		init();
	})();
}